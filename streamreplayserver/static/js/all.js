var socket = io.connect('/websocket');

socket.on('connect', function () {
    console.info('connect');
});
socket.on('connect_error', function (error) {
    console.info('connect_error');
});
socket.on('disconnect', function (reason) {
    console.info('disconnect');
});

socket.io.on('reconnect', function (attemptNumber) {
    console.info('reconnect');
});
socket.io.on('reconnect_error', function (error) {
    console.info('reconnect_error');
});
socket.io.on('reconnect_failed', function () {
    console.info('reconnect_failed');
});
socket.io.on('error', function (error) {
    console.info('error');
});
