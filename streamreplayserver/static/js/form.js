function video_control(channel, track, action, value) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/control/" + channel + "/" + track + "/" + action + "/" + value, true);
    xhttp.send();
}

function synced_scrub(channel, track, time) {
    video_control(channel, track, 'seek', videojs.getPlayer('replay-video').currentTime() + parseInt(time));
}

function synced_control(channel, track, action, value) {
    synced_scrub(channel, track, 0);
    video_control(channel, track, action, value);
}
