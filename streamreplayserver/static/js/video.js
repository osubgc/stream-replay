socket.on('video_control', function (msg) {
    console.info('video_control ', msg);

    if (msg['channel'] === replay_channel && msg['track'] === replay_track) {
        switch(msg['action']) {
            case "play":
                videojs.getPlayer('replay-video').play();
                break;
            case "pause":
                videojs.getPlayer('replay-video').pause();
                break;
            case "live":
                videojs.getPlayer('replay-video').currentTime(videojs.getPlayer('replay-video').liveTracker.lastSeekEnd_ - parseInt(msg['value']));
                break;
            case "speed":
                videojs.getPlayer('replay-video').playbackRate(parseFloat(msg['value']));
                break;
            case "scrub":
                videojs.getPlayer('replay-video').currentTime(videojs.getPlayer('replay-video').currentTime() + parseInt(msg['value']));
                break;
            case "seek":
                videojs.getPlayer('replay-video').currentTime(parseInt(msg['value']));
                break;
            default:
                console.info('unsupported action', msg['action']);
        }
    }
});
