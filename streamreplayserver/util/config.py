import json


def get_config():
    with open("config.json") as json_data:
        return json.load(json_data)
